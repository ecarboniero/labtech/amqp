package io.labtech.amqp.producer.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ProduceMessage {
    private static final Logger log = LoggerFactory.getLogger(ProduceMessage.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private final AmqpTemplate amqpTemplate;

    public ProduceMessage(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    @Scheduled(fixedRate = 1000)
    public void send() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setAppId("labtech");
        Message message = new Message("testo".getBytes(StandardCharsets.UTF_8), messageProperties);
        amqpTemplate.send("labtech-direct-exchange","routingkey", message);
    }
}

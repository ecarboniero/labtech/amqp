package io.labtech.amqp.consumer.listner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class MessageListner {
    private static final Logger log = LoggerFactory.getLogger(MessageListner.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @RabbitListener(queues="labtech")
    public void listen(Message message) {
        log.info("The time is now {}", dateFormat.format(new Date()));
        log.info("The message body {}", message.getBody().toString().toUpperCase());
    }
}
